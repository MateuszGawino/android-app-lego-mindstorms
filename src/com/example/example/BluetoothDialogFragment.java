package com.example.example;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class BluetoothDialogFragment extends DialogFragment {
	
	private ArrayList<String> items = new ArrayList<String>();
	private ArrayAdapter<String> adapter;
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final MainActivity mainActivity = (MainActivity) getActivity();
		AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        
        adapter = new ArrayAdapter<String>(getActivity(),
        		android.R.layout.simple_expandable_list_item_1,
        		items);
        // TODO- listenery do poprawy wg tutoriala?
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            	mainActivity.connect(id);
            	mainActivity.finishSearchingForDevices();
            	clear();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            	mainActivity.finishSearchingForDevices();
            	clear();
            }
        });
        return builder.create();
    }
	
	public void add(String s) {
		items.add(s);
		adapter.notifyDataSetChanged();
	}
	
	public void clear() {
		items.clear();
		adapter.notifyDataSetChanged();
	}
}
