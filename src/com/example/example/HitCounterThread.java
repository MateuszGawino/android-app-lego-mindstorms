package com.example.example;

import java.io.DataInputStream;
import java.io.IOException;

public class HitCounterThread extends Thread {
	
	private static final char HIT = 'h';
	private static final int HEALTH = 10;
	
	private int numberOfHits;
	private DataInputStream inputStream;
	private MainActivity mainActivity;
	
	public HitCounterThread(DataInputStream inputStream,
							MainActivity mainActivity) {
		this.numberOfHits = 0;
		this.inputStream = inputStream;
		this.mainActivity = mainActivity;
	}
	
	@Override
	public void run() {
		mainActivity.setHitText("Hits:" + System.getProperty("line.separator") + numberOfHits);
		char message;
		for (int i = 0; i < HEALTH; i++) {
			try {
				message = inputStream.readChar();
				if (message == HIT) {
					numberOfHits++;
					mainActivity.setHitText("Hits:" + 
											System.getProperty("line.separator") + 
											numberOfHits);
				}
			} catch (IOException e) {
				mainActivity.setHitText(e.getMessage());
			}
		}
		
		mainActivity.setHitText("You lose!");
		// TODO nie zamyka streama, bo nxt sie wywala- nie ma z tym problemu?
	}
}
