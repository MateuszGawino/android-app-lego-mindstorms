package com.example.example;

import java.io.DataOutputStream;
import java.io.IOException;


public class RobotPilot {
	
	private static final double MAX_SPEED = 6*1.2*110;
	private static final int MAX_ANGLE = 60;
	private static final char END = 'e';
	private static final char FORWARD = 'f';
	private static final char BACKWARD = 'b';
	private static final char TURN ='t';
	private static final char CHANGE_SPEED = 'c';
	private static final char START_GAME = 'g';
	private static final char STOP = 's';
	
	private DataOutputStream outputStream;
	private int lastAngle = 0;
	
	
	RobotPilot(DataOutputStream outputStream) {
		this.outputStream = outputStream;
	}
	
	public void sendCommand(char operation, int value) throws IOException {
		outputStream.writeChar(operation);
		outputStream.writeInt(value);
		outputStream.flush();
	}
	
	public void disconnect() throws IOException {
		sendCommand(END, 0);
		outputStream.close();
	}
	
	public void startGame() throws IOException {
		sendCommand(START_GAME, 0);
	}
	
	public void changeSpeed(double speed) throws IOException {
		int speedValue = (int) (Math.abs(speed) * MAX_SPEED);
		sendCommand(CHANGE_SPEED, speedValue);
	}
	
	public void changeMove(int forward, double angle) throws IOException {
		char operation;
		int angleValue = (int) (angle * MAX_ANGLE);
		if (forward == 0)
			operation = STOP;
		else if (forward > 0)
			operation = FORWARD;
		else
			operation = BACKWARD;
		
		// TODO nie przesylac wartosci bez sensu- do poprawy
		sendCommand(operation, 0);
		
		if (Math.abs(angleValue - lastAngle) > 2) {
			lastAngle = angleValue;
			sendCommand(TURN, angleValue);
		}
	}
}
