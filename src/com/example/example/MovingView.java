package com.example.example;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceView;

public class MovingView extends SurfaceView {
	
	private static final float circleRadius = 150;
	private static final float pointRadius = 5;
	private PointF middle;
	private PointF touch;
	private Paint pointPaint = new Paint();
	private Paint circlePaint = new Paint();
	
	public interface OnMoveListener {
		public void onChangeMove(int forward, double angle);
	}
	
	private OnMoveListener listener;
	
	public void setOnMoveListener(OnMoveListener listener) {
		this.listener = listener;
	}

	public MovingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public MovingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		pointPaint.setColor(Color.BLACK);
		circlePaint.setColor(Color.RED);
		circlePaint.setStyle(Paint.Style.STROKE);
	}

	public MovingView(Context context) {
		super(context);
	}
	
	private double getDistance(float x, float y) {
		return Math.sqrt(Math.pow(x - middle.x, 2) + Math.pow(y - middle.y, 2));
	}
	
	private double getAngle() {
		double angle = Math.asin((touch.x - middle.x) / getDistance(touch.x, touch.y));
		return ((2 * angle) / Math.PI); 
	}
	
	private boolean forward() {
		return middle.y >= touch.y;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float newX = event.getX();
		float newY = event.getY();
		double distance = getDistance(newX, newY);
		if (distance > circleRadius) {
			float ratio = (float) (circleRadius / distance);
			newX = middle.x + ratio * (newX - middle.x);
			newY = middle.y + ratio * (newY - middle.y);
		}
		touch = new PointF(newX, newY);
		
		switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
			{
				if(listener != null)
					listener.onChangeMove(0,0);
				touch = middle;
				invalidate();
				break;
			}
			default:
			{   
				int forward = forward() ? 1 : (-1);
				if(listener != null)
					listener.onChangeMove(forward, getAngle());
				invalidate();
				break;
			}
		}
		return true;
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		middle = new PointF(w/2,h/2);
		touch = middle;
		super.onSizeChanged(w, h, oldw, oldh);
	}

	protected void onDraw(Canvas canvas) {
		canvas.drawCircle(middle.x, middle.y, pointRadius, pointPaint);
		canvas.drawCircle(touch.x, touch.y, pointRadius, pointPaint);
		canvas.drawCircle(middle.x, middle.y, circleRadius, circlePaint);
		canvas.drawLine(middle.x, middle.y, touch.x, touch.y, pointPaint);
    }
	
}
