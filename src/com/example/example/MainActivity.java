package com.example.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends FragmentActivity {
	
	private static final String UID = "00001101-0000-1000-8000-00805F9B34FB";
	private static final String DEVICE_NAME = "NXT";
	private static final int REQUEST_ENABLE_BT = 1023;
	
	private MovingView movingView;
	private RobotPilot pilot;
	private TextView speedView;
	private TextView angleView;
	private TextView btInfo;
	private TextView gameStatus;
	private SeekBar speedBar;
	private Button startButton;
	private BluetoothSocket nxtSocket;
	private BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	private BluetoothDialogFragment bluetoothDialog = new BluetoothDialogFragment();
	private ArrayList<BluetoothDevice> availableDevices = new ArrayList<BluetoothDevice>();
	private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
	    public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	            bluetoothDialog.add(device.getName() + " " + device.getAddress());
	            availableDevices.add(device);
	        }
	    }
	};
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == RESULT_CANCELED) {
	        	btInfo.setText("Bluetooth connection cancelled");
	        }
	    }
	}
	
	public void searchForBluetoothDevices() {
		bluetoothDialog.show(getSupportFragmentManager(), "BluetoothDialogFragment");
		adapter.startDiscovery();
	}
	
	public void startBluetoothDiscovery(View view) {
		if (adapter == null) {
			btInfo.setText("Device doesn't support Bluetooth");
			return;
		}
		if (!adapter.isEnabled()) {
			Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBluetooth, REQUEST_ENABLE_BT);
		} else {
			searchForBluetoothDevices();	
		}
		
	}

	public void finishSearchingForDevices() {
		adapter.cancelDiscovery();
		availableDevices.clear();
	}
	
	public void setHitText(String s) {
		final String text = s;
		gameStatus.post(new Runnable() {
			@Override
			public void run() {
				gameStatus.setText(text);
			}
		});
	}
	
	public void startGame(View view) {
		if (pilot == null) {
			gameStatus.setText("Connection problem");
			return;
		}
		
		try {
			pilot.startGame();
			DataInputStream inputStream = new DataInputStream(nxtSocket.getInputStream());
			HitCounterThread hitCounter = new HitCounterThread(inputStream, this);
			hitCounter.start();
		} catch (IOException e) {
			gameStatus.setText(e.getMessage());
		}
	}
	
	public void connect(int deviceNumber) {
		DataOutputStream outputStream = null;

		if (deviceNumber < 0 || deviceNumber >= availableDevices.size()) {
			btInfo.setText("NXT not found");
			return;
		}
		
		BluetoothDevice nxtDevice = availableDevices.get(deviceNumber);
		
		if (nxtDevice.getName() == null || !nxtDevice.getName().equals(DEVICE_NAME))
		{
			btInfo.setText("Select an NXT");
			return;
		}
		
		btInfo.setText("Connecting...");
		try {
			nxtSocket = nxtDevice.createRfcommSocketToServiceRecord(UUID.fromString(UID));
			adapter.cancelDiscovery();
			nxtSocket.connect();
			outputStream = new DataOutputStream(nxtSocket.getOutputStream());
			btInfo.setText("Connected");
			startButton.setEnabled(true);
		} catch (Exception e) {
			btInfo.setText(e.getMessage());
		}

		if (outputStream != null) {
			pilot = new RobotPilot(outputStream);
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		speedView = (TextView) findViewById(R.id.textView1);
		angleView = (TextView) findViewById(R.id.textView2);
		btInfo = (TextView) findViewById(R.id.textView3);
		gameStatus = (TextView) findViewById(R.id.textView4);
		speedBar = (SeekBar) findViewById(R.id.seekBar1);
		startButton = (Button) findViewById(R.id.button2);
		startButton.setEnabled(false);
		movingView = (MovingView) findViewById(R.id.movingView1);
		movingView.setOnMoveListener(new MovingView.OnMoveListener() {
			@Override
			public void onChangeMove(int forward, double angle) {
				try {
					if (pilot != null)
						pilot.changeMove(forward, angle);
				} catch (IOException e){
					btInfo.setText("Moving failed");
				}
				angleView.setText("Angle:" + System.getProperty("line.separator") + String.format("%.2f", angle));
			}
		});
		speedBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				double speed = seekBar.getProgress();
				speed = speed / seekBar.getMax();
				try {
					if (pilot != null)
						pilot.changeSpeed(speed);
				} catch (IOException e) {
					btInfo.setText("Changing speed failed");
				}
				speedView.setText("Speed:" + System.getProperty("line.separator") + String.format("%.2f", speed));
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
										boolean fromUser) {
				double speed = seekBar.getProgress();
				speed = speed / seekBar.getMax();
				speedView.setText("Speed:" + System.getProperty("line.separator") + String.format("%.2f", speed));
			}
		});
		speedBar.setProgress(speedBar.getMax() / 2);
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(bluetoothReceiver, filter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(bluetoothReceiver);
		try {
			pilot.disconnect();
			Thread.sleep(200);
			nxtSocket.close();
		} catch (Exception e) {
			btInfo.setText("Disconnect failed");
		}
		super.onDestroy();
	}
	
	
}
